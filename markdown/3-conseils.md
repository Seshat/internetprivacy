<section><section><h1>Conseils</h1>

![](img1/bonnet.jpg)

---

Outils
-------

[https://www.privacytools.io](https://www.privacytools.io) :

![](img1/tools.png){width=600px}

---

Système d'exploitation
----------------------

-   Système d'exploitation et logiciels toujours **mis à jour**
-   Système propre &rarr; **antivirus actif** (surtout sous <b>Windows</b>)
-   <b>Linux</b> **moins risqué** que <b>Mac OS</b> **moins risqué**  que <b>Windows</b>
-   En général, **éviter** <b>Windows</b> (&larr; collecte d'info ou utiliser [ça](https://www.w10privacy.de/english-home/))
-   Compiler soi-même un noyau <b>Linux</b>, l'encrypter et mettre la partie du noyau qui décrypte sur une clé USB
-  **Meilleur** : [<b>TAILS</b>](https://tails.boum.org), utilisé par Edward Snowden
    S'installe sur une clé USB, connexion internet anonyme, aucune trace

![](img1/tails.jpg)

---

Mots de passe 
-------------
Mots de passe faciles à trouver :

-   Ceux de la liste disponible
    [ici](https://www.protegez-vous.ca/Nouvelles/Technologie/les-15-types-de-mots-de-passe-les-plus-repandus-sur-le-web)

-   Ceux basés sur dictionnaire
  
-   Caractères aléatoires (>12 en 2021)
  
&rarr; **Quelques secondes**

&rarr; **Longueur mieux que complexité**

- Tester [ici]( https://www.ssi.gouv.fr/administration/precautions-elementaires/calculer-la-force-dun-mot-de-passe/) ou [ici](https://pwdtest.bee-secure.lu/)

---

- **+ de 12** caractères aléatoires, ou liste mots
-   Pas commun à plusieurs comptes
-   Changer régulièrement
-   Logiciels dédiés dans des containers chiffrés

    Ex : <https://www.keepassx.org>

![](img1/kpx.png)

---

Suppression des données
------------------------
-   Lorsqu'on efface un fichier, **Seule sa référence disparait**
-   Le contenu est **toujours sur le disque**
-   Des outils ([BleachBit](https://www.bleachbit.org/),
    [DBAN](https://dban.org/),[Eraser](https://eraser.heidi.ie)) écrasent les données

    &rarr; La suppression est rarement parfaite

-   Pour effacement total : utilisation d'un champ magnétique lourd

<video loop autoplay>
<source src="img1/MrRobot.mp4" type="video/mp4">
</video>

---

Chiffrement et MAC Spoofing 
---------------------------

<blockquote class="q">Le cryptage est une responsabilité civique, un devoir civique <span>Edward Snowden</span></blockquote> 

-   **Outils de Chiffrement intégrés** dans les OS
-   Pour aller plus loin, des logiciels comme
    [EncFS](https://vgough.github.io/encfs/),
    [VeraCrypt](https://www.veracrypt.fr/en/Home.html) ou des dérivés de <B>TrueCrypt</B>
- Cryptée un fichier :
```
tar -cz your_dir | gpg -c -o your_archive.tgz.gpg
```
Déballez le:
```
gpg -d your_archive.tgz.gpg | tar xz
```

---

MAC Spoofing
------------

-   L'adresse MAC est l'identifiant d'une carte réseau

-   Intervient dans processus pour obtenir adresse IP

-   Est utilisée pour être tracé

    &rarr; Utilitaires pour modifier l'adresse MAC :

    -   `macchanger` sous <b>Linux</b>

    -   [poofMAC](https://github.com/feross/SpoofMAC) pour tous les OS

---

Emails
------

-   Usage de l'adresse perso aux personnes en qui on a confiance (amis,
    banques, etc)

-   **Utilisation de plusieurs adresses** "poubelle" pour le reste

    &rarr; Brouiller les pistes

  **&#9888;**  Fournisseurs comme <b>Gmail</b> sait faire le lien entre nos comptes

-   Envoyer et recevoir des emails chiffrés, même si rien à cacher

    Ex : avec [GPG](https://www.gnupg.org) ou avec
    [ProtonMail](https://protonmail.com)

-   Clients mails peuvent présenter des risques
    
 Privilégier l'open source (ex : <b>Thunderbird</b>)

---

Précautions simples
--------------------

-   Comprendre comment l'informatique fonctionne, suivez des MOOC,**suivez des sites sur la sécurité** (<http://www.zataz.com>, 
    [cyber-warzone](https://www.undernews.fr/cyber-warzone))

-   Utilisez un et hautement configurable comme <b>Firefox, Chromium, ou PaleMoon</b>
    
-  **Utilisez des plug-ins ** comme <b>AdBlock Plus ou Block Origin</b> pour bloquer des publicités tierces sur les sites que vous consultez.
  
-   Utilisation de la **Navigation privé** &rarr; protège l'historique de navigation

- **Chiffrer** son disque dure

---

A quoi sert la navigation privée ?
----------------------------------

-   Activable depuis n'importe quel navigateur

-   Permet de ne pas enregistrer certaines informations :

    -   L'**historique** des sites visités,

    -   Vos **mots de passe**,

    -   Les champs d'un **formulaire** que vous remplissez,

    -   Les **cookies traceurs** déposés par les sites que vous avez visités.

-   Sont néanmoins enregistrés :

    -   Les sites que vous avez enregistrés dans vos **favoris**

    -   Les **téléchargements effectués** dans le dossier ```téléchargements```

---

Tester son navigateur 
----------------------
Utiliser anopticlick :
<https://panopticlick.eff.org/>

![](img1/chrome.png)

Avec <b>Chrome</b>

---

Navigateurs plus sûr

![](img1/panopticlick.jpg)

-   [Epic Privacy Browser](https://epicbrowser.com)
-   [UR](https://www.ur-browser.com)
-   [Iridium browser](https://iridiumbrowser.de)

---

Précautions plus contraignantes
-------------------------------

-   N'utiliser que des logiciels **OpenSource** &rarr; plus de vérifications et moins de contrôle
-   Plugin de navigateurs:
    -   [Ghostery](<https://www.ghostery.com>) ou DoNotTrackMe pour **bloquer les cookies**
    -   <b>NoScript</b> pour bloquer sélectivement les scripts
-   Utilisation de :
    -   <https://DuckDuckGo.com>
    -   <https://Qwant.com>
-   Utilisation d'un **VPN** *(non gratuit):*
    -   [Documentation d'installation de client
        VPN](https://wikidocs.univ-lorraine.fr/pages/viewpage.action?spaceKey=infra&title=VPN&src=search)

 **&#9888;** Certains sites Web ne fonctionneront plus pour vous

---

Précautions très contraignantes
-------------------------------
-   Rejoindre les différentes associations qui luttent contre la surveillance ([WikiLeak](https://wikileaks.org) ou la [Quadrature du Net](https://www.laquadrature.net))
-   Mettre ses données plutôt sur un <b>Pims</b> que sur des plateformes comme <b>Apple</b> ou <b>Google</b>
-   Utiliser <b>Signal</b> plutôt que <b>WhatsApp</b>, plutôt que <b>Messenger</b>
-   Utiliser plutôt <b>GnuSocial</b> que <b>Facebook</b>
-   Utiliser plutôt <b>Mastodon</b> que <b>Tweeter</b>
-   Ne pas utiliser de disque dur, juste <b>TAILS</b> sur une clé USB)
-   Masquer son IP (ex : avec le navigateur <b>Tor</b>), évitez le plein
    écran
-   Effectuer ses transactions financières en <b>Crypto-Monnaie</b> comme [Monero](https://www.getmonero.org/resources/moneropedia/bulletproofs.html) ([video avec BitCoin](https://www.youtube.com/watch?v=aXkO_Jhogtk))
-   Partez pour un village perdu sans Internet

---

Le dark web - V1
----------------
<div class="container">
<div class='col1'>
![](img1/MarianaWeb.jpg)
</div>
<div class='col2'>
<font size=5px>
<ul>
<li>  **Niveau 0 : Le Web commun** </li>
<ul>
<li> <b>YouTube, Facebook, Wikipedia,</b> etc.</li>
</ul>   
<li> **Niveau 1 : Le Web de Surface** 
<ul>
<li> Sites internet "sombres", services d'adresses Email temporaire, les bases de données <b>MYSQL</b>, etc </li>
</ul>  
<li> **Niveau 2 : Bergie Web** </li> 
<ul>
<li> Accéder avec un proxy / VPN, <b>Tor</b> ou en modifiant votre matériel</li>
</ul>  
<li> **Niveau 3 : Le Web profond**</li>
<ul>
<li>  Avec <b>Tor</b>: Drogue, Trafic d'êtres humains et les marchés noirs.</li>
<li>  Avec un système Shell fermé : la loi 13, les expériences de la
2ème guerre, etc.</li>
</ul>
<li> **Niveau 5 : Le Marianas Web** </li> 
<ul>
<li>Avec **falcighol dérivation polymère** &rarr; informatique quantique </li>
</ul>
</ul>
</font>
</div>
</div>

*Source : n° 256 de la revue de la Gendarmerie Nationale*

---

Le dark web -V2 
---------------

![](img1/deepWeb.jpg)


---

Références
----------

-   *<https://theconversation.com/a-la-recherche-des-donnees-perdues-70806>*
-   *<https://www.cnil.fr/fr/maitriser-mes-donnees>*
-   *<http://www.zataz.com>*
    -   *<http://www.zataz.com/vpn-la-protection-des-donnees-proposee-par-firefox/>*
    -   *<http://www.zataz.com/navigateur-ur-securiser-surf/>*
    -   *<http://www.zataz.com/iridium-browser-surfer-anonyme/>*
    -   *<http://www.zataz.com/epic-privacy-browser-protegez-navigation-web/>*
-   **Darknet Mythes et Réalités**, *Jean-Philippe Rennard*, Ellipses Marketing, 2016.


---

Bonus 
-----

![](cnil/les_10_conseils.png)


---

1. Réfléchissez avant de publier
------------------------
![](cnil/cnil1.jpg)

---

2. Respectez les autres 
-----------------------
![](cnil/cnil2.jpg)

---

3. Ne dites pas tout
----------------
![](cnil/cnil3.jpg)

---

4. Sécurisez vos comptes 
----------------
![](cnil/cnil4.jpg)

---

5. Créez plusieurs adresses e-mail
----------------
![](cnil/cnil5.jpg)

---

6. Attention aux photos et aux vidéos
----------------
![](cnil/cnil6.jpg)

---

7. Utilisez un pseudonyme 
----------------
![](cnil/cnil7.jpg)

---

8. Attention aux mots de passe
----------------
![](cnil/cnil8.jpg)

---

9. Faites le ménage dans vos historiques
----------------
![](cnil/cnil9.jpg)

---

10. Vérifiez vos traces 
----------------
![](cnil/cnil10.jpg)

---


[^6]: [Les données que Google
    conserve](https://www.francetvinfo.fr/internet/google/achats-deplacements-enregistrements-de-voix-j-ai-fouille-dans-les-donnees-que-google-conserve-sur-moi-depuis-treize-ans-et-rien-ne-lui-echappe_3254425.html)

[^7]: <http://www.smartsupervisors.com/>

[^8]: <https://www.youtube.com/watch?v=aXkO_Jhogtk>

</section></section>
