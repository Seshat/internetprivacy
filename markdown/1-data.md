
<section> <h1>Introduction</h1>

![](img1/facebook-employeur.jpg)

---

-   Nous n'avons rien à cacher
    -   Un peu moins de liberté pour un peu plus de **sécurité**
    -   Juste messages si **banals** ou mes photos de vacances

---

-   Les GAFAM (<b>Google</b>, <b>Apple</b>, <b>Facebook</b>, <b>Amazon</b>, <b>Microsoft</b>) et autres géants du web nous **facilitent** la vie
    -   Moteurs de recherche 
    -   Solutions de stockage &rarr; cloud computing
    -   Etc.

----

Ai-je des raisons de m'inquiéter ?

<div  class="check_ul">
<ul>
<li> Je ne suis pas quelqu'un de **suspect**. </li>
<li> Je veux qu'on me surveille pour me **protéger**. </li>
<li> Je pense que la **loi** doit toujours être respectée. </li>
<li> J'ai confiance en mon **gouvernement**. </li>
<li> Je n'ai pas à me cacher, je ne fais d'**illégal**.</li>
<li> Je suis prêt à tout pour ma **sécurité**! </li>
<li> Peu importe, tant que ce sont des **métadonnées**.</li>
</ul>
</div>

---

Plan
---

1.  Le contrôle des données
2.  Je n'ai rien à cacher !
3.  Quelques conseils


 </section> </section>

<section> <section><h1>Le contrôle des données</h1>

![](img1/data.jpg)

---

Création de données
-----

<ul>
<li class="fragment fade-up">   Utilisation de **logiciels**(ex: <b>Microsoft Word</b>)</li>
<li class="fragment fade-up">   **Surf** sur internet :
<ul>
<li>Sites visités, cliques, mots utilisés</li>
<li>Logiciels installés (lesquels, quand et avec quelles options) &rarr; [identification](<https://amiunique.org>)</li>
</ul>
</li>
<li class="fragment fade-up">   Communication
<ul>
<li><b>Facebook, Twitter, Instagram, SnapChat, WhatsApp,</b></li>
</ul>
</li>  
<li class="fragment fade-up">   Utilisation de **portables**
<ul>
<li>Appels, avec qui, quand, combien de temps, où</li>
<li>Localisation avec antenne la plus proche, GPS</li>
</ul>
</li>
<li class="fragment fade-up">   Utilisation de carte bleue</li>
<li class="fragment fade-up">  <b>Apple HomePod, Amazon Echo, Google Home</b></li>
</ul>

---

<ul>
<li class="fragment fade-up">   Caméra : magasin, distributeur, dans la rue</li>
<li class="fragment fade-up">   [Dans la voiture](https://www.lemonde.fr/m-actu/article/2015/10/02/la-voiture-cette-espionne_4781511_4497186.html) : utilisation du frein, pression des pneus</li>
<li class="fragment fade-up">   Appareil photo : position, date, heure, identification de
    l'appareil, ...</li>
<li class="fragment fade-up">   Toutes transactions économiques : magasins, Uber, autoroutes</li>
<li class="fragment fade-up">   Electroménager (ex: frigo, [aspirateur](https://www.theverge.com/2017/7/24/16021610/irobot-roomba-homa-map-data-sale), [robot cuiseur](https://www.numerama.com/tech/525214-monsieur-cuisine-connect-micro-cache-android-non-securise-les-dessous-du-robot-cuisine-de-lidl.html))</li>
<li class="fragment fade-up">   [Domotique](https://gizmodo.com/the-house-that-spied-on-me-1822429852?rev=1518027891546)</li>
<li class="fragment fade-up">  Trackers de fitness : bracelets, balances, \...</li>
<li class="fragment fade-up">  Montres GPS pour le sport</li>
<li class="fragment fade-up">   Liseuses (ex : <b>Kindle</b>)</li>
<li class="fragment fade-up">   Internet of things</li>
<li class="fragment fade-up">   Villes connectées (ex: [Toronto](https://www.sidewalklabs.com/dtpr/))</li>
<li class="fragment fade-up">   Toutes <b>apps</b> ou tout ce qui n'est pas OpenSource</li>
</ul>

---


Quantité de données
-------------------

-   Metadata, exemple :
    -   [Vos déplacements via
        Google](https://www.google.com/maps/timeline?authuser=0&pb)
    -   [Votre historique de
        recherche](https://myactivity.google.com/myactivity?product=27)
    -   [Votre historique
        Youtube](https://www.youtube.com/feed/history/search_history)

    &rarr; [Démo](https://www.francetvinfo.fr/internet/google/achats-deplacements-enregistrements-de-voix-j-ai-fouille-dans-les-donnees-que-google-conserve-sur-moi-depuis-treize-ans-et-rien-ne-lui-echappe_3254425.html) plus générale sur ce que stocke Google

<!-- -   Exemple de <b>Facebook</b> :
    -   En 2011 M. Schrems demande données sur lui
    -   1200 pages données + liens, photos, pubs où il a cliqué -->

-   Coût énergétique du stockage ([pollution cachée]( https://www.inriality.fr/environnement/ecologie/reseaux-data-centers-et/))
  
---


Surveillance des données
------------------------

-   Basée sur **metadonnées**(bien pour population)
-   Basée sur <b>Google</b>
    -   Connaît petits secrets
    -   Plus que soit-même
    -   Ex : autocomplete
-   Basée sur carnet d'adresse
    -   Ex : affiliation politique
    -   Ex : orientation sexuelle
-   Basée sur caméras
    -   Ex : reconnaissance de visages
    -   Ex : reconnaissance de plaques d'immatriculation
-   Etc
  
---

-   Intérêt pour les gouvernements les entreprises
-   **Coût** de la surveillance
    -   1 agent Stasi pour 66 civils (Cf *La Vie des Autres*)
    -   Prix des accessoires **&darr;**
-   Très accessible :
    -   Ex : [logiciels espions pour portables](http://www.smartsupervisors.com) (<b>Spy, smSpy, lexiSpy, pyera, asySpy</b>) &rarr; 1M d'utilisateurs

&rarr; On se rapproche du féodalisme

![](img2/medieval.jpg)

---

Analyse des données 
-------------------

Ex : Histoire d'une personne achetant des produits
où une IA détecte une grossesse

![](img1/target.jpg)

-   Type de données = **Big Data**
-   Extraction des données intéressantes = **Data Mining**
-   Basé sur du **Deep Learning**
  
---

Exemple de Data Mining
----------------------

![](img1/deepLearning.png){ width=800px }

&rarr; Est-ce qu'un client va acheter quelque chose de cher ?

- Va se marier ?
- Va en vacances ?
- Achète une maison ?

---


Chercher des relations
----------------------

-   Metadonnées collectées par NSA avec degré de relation
    -  Trouver des conspirations
    -  NSA en 2013 : 117675 cibles &rarr; 20M pers surveillées
-   Ex : <b>Facebook</b> montre **mêmes pub** aux amis des amis

![](img1/facebook.png)

---


Types de surveillance
---------------------

- Surveillance cachée
    -   Ex : Sites de commerce en ligne
    -   Ex : Voitures
- Surveillance automatique
    -   Ex : <b>Facebook</b> (likes, pris en photo par un tiers)
    -   Ex : <b>Gmail</b>, même sans adresse mail <b>Gmail</b>
    -   Ex : Mémorisation des mots de passe wifi par <b>Apple</b>
-  Surveillance de masse
    - Moins chère que surveillance individuelle
    - Caméras à Londres enregistrent tout
    - Metadonnées sur tous les portables
    - [Qualité des vidéos](http://www.gigavision.cn/index.html) &nearr; 

---

Gratuité
---------
-   Peu de choses sont réellement gratuites
-   Le moindre service a un **coût**
-   Le financement peut être basé sur:
<ul>
<li class="fragment fade-up"> Le **don** (ex : <b>Wikipédia</b>)</li>
<li class="fragment fade-up"> La **publicité**</li>
<li class="fragment fade-up"> Le **profilage** (acquisition de données utilisateurs) Ex : WiFi gratuit (hôtels, cafés, boutiques, ...)</li>
</ul>

![](img1/free.jpg)

---

Les cookies
-----------
-   Petit fichier texte stocké chez l'internaute
-   &rarr; Faciliter la navigation
-   **Info perso** pouvant être exploitées par des tiers
-   Même en évitant <b>Google</b>, sites utilisent <b>Google Analytics</b>
-   Il est possible de les visualiser avec
    [cookieviz](https://linc.cnil.fr/fr/cookieviz-une-dataviz-en-temps-reel-du-tracking-de-votre-navigation)

![](img1/cookieviz.jpg)

---

Usurpation d'identité 
--------------------
-   En utilisant les cookies
-   En surveillant l'activité sur les réseaux sociaux

Trouver l'information nécessaire pour :

- Ouvrir un compte bancaire
- Prendre une location
- Obtenir des papiers au nom de la victime

&rarr; **Difficile de sortir rapidement indemne**

![](img1/identity_thief.jpg)

---

Société exhibitionniste
--------------------
-   Voyeurisme &rarr; très naturel (Cf *Fenêtre sur cours*)
-   Goût impudique de se montrer
    -   Webcam **&uarr;** nombreux internautes partagent leur intimité
        24h/24
-   Journaux intimes **&uarr;** (ex : [le journal de mort](https://www.theguardian.com/technology/2000/dec/19/healthsection.lifeandhealth) par Lu Youqing)
-   Téléréalité **&uarr;**
-   Devenir des *indics* (ex : [observer la frontière mexicaine](http://www.blueservo.com))

![](img1/s1.jpg) ![](img1/s2.jpg) ![](img1/s3.jpg) ![](img1/s4.jpg)

---

Google - plus d'un milliard d'utilisateurs
----------------------------------------
-   <b>Google Search</b> : lieu, date, objet de la recherche
-   <b>Google Chrome</b> : ce qui est fait en matière de navigation
-   <b>Google Analytics</b> : navigation de l'internaute sur le web
-   <b>Google Currents</b> : complément d'information qu'il recoupe
-   <b>GMail</b> : analyse la correspondance
-   <b>YouTube</b> : enregistre tout
-   <b>Google Maps</b> : identifie où vous vous trouvez et quand
-   <b>Google adWords</b> : ce que vous voulez vendre ou promouvoir
-   <b>Google Android</b> : où vous êtes et ce que vous y faites
   
&rarr; Informe le gouvernement américain et vend ces données

Référence :
[http://blog.axe-net.fr/\...](http://blog.axe-net.fr/google-analyse-comportement-internaute/)

---

Perte de la neutralité du web
------------------------------

![](img1/trinet.png)

-   <b>Google, Facebook et Amazon</b> se sont accaparés le Web
    -   <b>Facebook</b> = le **social** (+<b>Whatsapp, Instagram</b>...)
    -   <b>Facebook</b> = source de trafic n°1 pour les médias
    -   <b>Google</b> = l'**IA**
    -   <b>Amazon</b>: part de marché en e-commerce US > 50%

---

Risques 
--------

-   Avoir un forfait internet uniquement avec <b>Google, Facebook et Amazon</b>
-   Perdre l'anonymat sur le web
-   Plus de pages web mais des pages <b>Facebook</b>
-   Sites de e-commerce rachetés par <b>Amazon</b>
-   Plus de web, juste internet pour le "**trinet**"

---

Polarisation
------------

- Dérives des algorithmes d'IA avec efficacité **&uarr;**
- **Objectif** : maintenir « engagés » sur <b>Facebook, YouTube, Instagram ou Twitter</b>. 

![](img1/polar.gif)

---

- **Impacts** : 
    - Polarisation des débats
    - Bulles de filtres qui nous mettent uniquement en relation avec des gens qui pensent comme nous
    - Addiction
    - Prime à la désinformation
    - Dépression

---

Responsabilités en tant que développeur
-----------

- Faire **une place à la sécurité** dans les discussions d'entreprise
    - Les **données**
    - La vérification de l'**authentification**
    - L'**absence de "confiance"** par défaut aux codes extérieurs

- Mettre en place **des tests** de comportements indésirables

- Prendre **le temps nécessaire** de s'occuper de la sécurité et de la vie privée **dès le début** d'un projet

&rarr; Développer *par design* **sans** collecte d'information

&rarr; Contribuer à rendre les applications **plus privées**   

Source : [Staying ahead in the cybersecurity Game](https://www.capgemini.com/wp-content/uploads/2017/07/staying_ahead_in_the_cybersecurity_game_-_what_matters_now.pdf)



Références
----------

-   **L'empire de la surveillance**, *Ignacio Ramonet*, Galilée, 2015.
-   **Data and Goliath: The Hidden Battles to Collect Your Data**,*Bruce Schneier*, WW Norton & Co,2016
-   **surveillance://**, *Tristan Nitot*, C&F Editions, 2016
-   **Guide Juridique du RGPD**, *Gérard HAAS*, ENI, 2018

</section></section>