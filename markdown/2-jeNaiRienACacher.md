<section><section> <h1>Vie privée</h1>

Moi\...

![](img1/rienacacher.jpg)

Je n'ai rien à cacher ! 

---

La Grande Collecte pour l'agrégation
------------------------------------

Le **programme étatsunien PRISM** (NSA) agrège et exploite les données collectées par :

<b> Microsoft, Google, Yahoo !, Facebook, YouTube, Skype, Apple, Dropbox, Discord, ...
</b>

---

Leur philosophie
----------------
<blockquote class="q">Si vous souhaitez que personne ne soit au courant de certaines des choses que vous faites, **peut-être ne devriez-vous tout simplement pas les faire**. <span>Eric Schmidt (Google)<span></blockquote>

<blockquote class="q">**Si vous n'avez rien à vous reprocher**, vous n'avez pas à avoir peur d'être filmés !<span>Brice Hortefeux UMP<span></blockquote>

<blockquote class="q">**Si on n'a rien à cacher**, il n'y a pas de problème
à être écouté.<span>Benoît Hamon PS<span></blockquote>

---

Je n'ai rien à cacher, mais\...

<font size=38>
**Questionnaire**
</font>

 *Ai-je des raisons de m'inquiéter ?*

![](img/nsa.png)

---

Ai-je des raisons de m'inquiéter ?

<div  class="check_ul">
<ul>
<li> Je ne suis pas quelqu'un de **suspect**. </li>
<li> Je veux qu'on me surveille pour me **protéger**. </li>
<li> Je pense que la **loi** doit toujours être respectée. </li>
<li> J'ai confiance en mon **gouvernement**. </li>
<li> Je n'ai pas à me cacher, je ne fais d'**illégal**.</li>
<li> Je suis prêt à tout pour ma **sécurité**! </li>
<li> Peu importe, tant que ce sont des **métadonnées**.</li>
</ul>
</div>

---

Etre suspect
------------

Ai-je des raisons de m'inquiéter ?

<ul class="noBullet">
<li><input  type="checkbox" id="option1"/>
<label for="option1"> <span></span> Je ne suis pas quelqu'un de suspect. </label></li>
</ul>

---

Rapport de la CNIL en 2008 sur le STIC : **1 million de personnes** blanchies mais pour **toujours suspectes** la police, fautes de frappe, homonymies, etc. 

- **Acheter** une cocotte minute et un sac d'engrais
la même semaine **est suspect**. 

- Utiliser **un VPN est suspect**.

---

Ai-je des raisons de m'inquiéter ?

<ul class="noBullet">
<li><input  type="checkbox" id="option2" checked/>
<label for="option2"> <span></span> Je ne suis pas quelqu'un de suspect. </label></li>
<li><input  type="checkbox" id="option3"/>
<label for="option3"> <span></span> Je veux qu’on me surveille pour me protéger. </label></li>
</ul>

---

La surveillance
---------------

Je veux qu'on me surveille pour me protéger

- <q>15% du temps sur les écrans de contrôle serait du **voyeurisme**</q>
(Noé Le Blanc) 

- **Les policiers eux-mêmes refusent** d'être surveillés. 

- La surveillance est passive et enregistre des gens qui **apprennent à ne pas pouvoir être reconnus**.

- L'absence de confidentialité **m’interdit de témoigner** sans me mettre en péril.

---

Ai-je des raisons de m'inquiéter ?

<ul class="noBullet">
<li><input  type="checkbox" id="option4" />
<label for="option4"> <span></span> Je ne suis pas quelqu'un de suspect. </label></li>
<li><input  type="checkbox" id="option5" checked/>
<label for="option5"> <span></span> Je veux qu’on me surveille pour me protéger. </label></li>
<li><input  type="checkbox" id="option6" />
<label for="option6"> <span></span> Je pense que la loi doit toujours être respectée. </label></li>
</ul>

---

Respecter la loi
----------------

- Pourtant, je ne suis pas d'accord avec **les règles d’autrefois**.
   
- À quoi ressembleront la société et les lois **dans 20 ans** ?
  
- Comment **mes propos d’aujourd’hui** seront-il interprétés demain ? 
  
- Comment la loi peut-elle **évoluer en fonction de mœurs ** si elle est incontournable ?

---

Ai-je des raisons de m'inquiéter ?

<ul class="noBullet">
<li><input  type="checkbox" id="option7" />
<label for="option7"> <span></span> Je ne suis pas quelqu'un de suspect. </label></li>
<li><input  type="checkbox" id="option8"/>
<label for="option8"> <span></span> Je veux qu’on me surveille pour me protéger. </label></li>
<li><input  type="checkbox" id="option9" checked/>
<label for="option9"> <span></span> Je pense que la loi doit toujours être respectée. </label></li>
<li><input  type="checkbox" id="option10"/>
<label for="option10"> <span></span> J'ai confiance en mon gouvernement. </label></li>
</ul>

---

Confiance au gouvernement
-------------------------

- L'appareil de surveillance sera récupéré par **les prochains gouvernements**, fascistes ou non. 

- Exemple avec l'Afghanistan [[1](https://www.20minutes.fr/monde/afghanistan/3107523-20210823-depuis-prise-pouvoir-talibans-influenceurs-afghans-disparu-reseaux-sociaux)], [[2](https://www.wired.co.uk/article/afghanistan-social-media-delete)], [[3](https://www.vice.com/en/article/v7enzb/the-us-is-removing-records-of-its-war-in-afghanistan-from-the-internet)]

- Ai-je confiance dans  **les autres gouvernements** (USA), que je ne peux pas élire et qui appliquent des lois sur lesquelles je n'ai **aucun contrôle** ?

---

Ai-je des raisons de m'inquiéter ?

<ul class="noBullet">
<li><input  type="checkbox" id="option11" />
<label for="option11"> <span></span> Je ne suis pas quelqu'un de suspect. </label></li>
<li><input  type="checkbox" id="option12"/>
<label for="option12"> <span></span> Je veux qu’on me surveille pour me protéger. </label></li>
<li><input  type="checkbox" id="option13"/>
<label for="option13"> <span></span> Je pense que la loi doit toujours être respectée. </label></li>
<li><input  type="checkbox" id="option14" checked/>
<label for="option14"> <span></span> J'ai confiance en mon gouvernement. </label></li>
<li><input  type="checkbox" id="option13"/>
<label for="option13"> <span></span>  Je n'ai pas
à me cacher, je ne fais rien d'illégal.</label></li>
    
</ul>

---

Ne rien faire d'illégal
-----------------------

- C'est illégal quand  **je m’isole pour téléphoner** ?
  
- **Mon journal intime** cadenassé était-il illégal ? 
  
- Que se passe-t-il si je suis tenu au **secret professionnel** ? 
  
<blockquote class="q">La vie privée, c'est comme un préservatif : si vous
n'en utilisez pas, **vous mettez aussi votre partenaire en
Conseils danger**.<span>Jacob Appelbaum</span></blockquote> 

---

Ai-je des raisons de m'inquiéter ?

<ul class="noBullet">
<li><input  type="checkbox" id="option14" />
<label for="option14"> <span></span> Je ne suis pas quelqu'un de suspect. </label></li>
<li><input  type="checkbox" id="option15"/>
<label for="option15"> <span></span> Je veux qu’on me surveille pour me protéger. </label></li>
<li><input  type="checkbox" id="option16"/>
<label for="option16"> <span></span> Je pense que la loi doit toujours être respectée. </label></li>
<li><input  type="checkbox" id="option17" />
<label for="option17"> <span></span> J'ai confiance en mon gouvernement. </label></li>
<li><input  type="checkbox" id="option18" checked/>
<label for="option18"> <span></span>  Je n'ai pas
à me cacher, je ne fais rien d'illégal.</label></li>
<li><input  type="checkbox" id="option19" />
<label for="option19"> <span></span> Je suis prêt à tout pour ma sécurité ! </label></li>
</ul>

---

La sécurité
-----------

- D'accord pour **noter chaque rencontre, discussion, déplacement** et fournir des **rapports papiers** à mon employeur ? 

- D'accord pour que **le facteur ouvre mes courriers, les lise, les photocopie** et garde une copie à vie ? 
  
<blockquote class="q"> Une attaque terroriste ne peut pas détruire le mode de vie d'un pays ; **notre réaction à cette attaque**, par contre, peut entraîner ce type de dommage.<span>Bruce Schneier</span></blockquote> 

- Quid des photocopies **volées et mal protégées** ?

---

Ai-je des raisons de m'inquiéter ?

<ul class="noBullet">
<li><input  type="checkbox" id="option20" />
<label for="option20"> <span></span> Je ne suis pas quelqu'un de suspect. </label></li>
<li><input  type="checkbox" id="option21"/>
<label for="option21"> <span></span> Je veux qu’on me surveille pour me protéger. </label></li>
<li><input  type="checkbox" id="option22"/>
<label for="option22"> <span></span> Je pense que la loi doit toujours être respectée. </label></li>
<li><input  type="checkbox" id="option23" />
<label for="option23"> <span></span> J'ai confiance en mon gouvernement. </label></li>
<li><input  type="checkbox" id="option24" />
<label for="option24"> <span></span>  Je n'ai pas
à me cacher, je ne fais rien d'illégal.</label></li>
<li><input  type="checkbox" id="option25" checked/>
<label for="option25"> <span></span> Je suis prêt à tout pour ma sécurité ! </label></li>
<li><input  type="checkbox" id="option26" />
<label for="option26"> <span></span>  Peu importe, tant que ce sont des métadonnées.</label></li>
</ul>

---

Les métadonnées
---------------

- **12 points** pour identifier quelqu'un avec ses empreintes digitales\... **seulement 4** (pour 95% des gens) via une base opérateur. 

- Les métadonnées servent à **construire et enrichir des graphes sociaux** pour chacun d'entre nous. 

- La NSA **surveille systématiquement ceux qui communiquent** avec un suspect. 

- Nous sommes **tous à 6 poignées de mains** l'un de l'autre
(**moins de 4 relations** sur <b>Facebook/Twitter</b>).

---

Ai-je des raisons de m'inquiéter ?

<ul class="noBullet">
<li><input  type="checkbox" id="option27" />
<label for="option27"> <span></span> Je ne suis pas quelqu'un de suspect. </label></li>
<li><input  type="checkbox" id="option28"/>
<label for="option28"> <span></span> Je veux qu’on me surveille pour me protéger. </label></li>
<li><input  type="checkbox" id="option29"/>
<label for="option29"> <span></span> Je pense que la loi doit toujours être respectée. </label></li>
<li><input  type="checkbox" id="option30" />
<label for="option30"> <span></span> J'ai confiance en mon gouvernement. </label></li>
<li><input  type="checkbox" id="option31" />
<label for="option31"> <span></span>  Je n'ai pas
à me cacher, je ne fais rien d'illégal.</label></li>
<li><input  type="checkbox" id="option32" />
<label for="option32"> <span></span> Je suis prêt à tout pour ma sécurité ! </label></li>
<li><input  type="checkbox" id="option33" checked/>
<label for="option33"> <span></span>  Peu importe, tant que ce sont des métadonnées.</label></li>
</ul>


---

Ai-je des raisons de m'inquiéter ?

<div  class="check_ul">
<ul>
<li> Je ne suis pas quelqu'un de **suspect**. </li>
<li> Je veux qu'on me surveille pour me **protéger**. </li>
<li> Je pense que la **loi** doit toujours être respectée. </li>
<li> J'ai confiance en mon **gouvernement**. </li>
<li> Je n'ai pas à me cacher, je ne fais d'**illégal**.</li>
<li> Je suis prêt à tout pour ma **sécurité**! </li>
<li> Peu importe, tant que ce sont des **métadonnées**.</li>
</ul>
</div>

---

Ai-je des raisons de m'inquiéter ?

<div  class="uncheck_ul">
<ul>
<li> Je ne suis pas quelqu'un de **suspect**. </li>
<li> Je veux qu'on me surveille pour me **protéger**. </li>
<li> Je pense que la **loi** doit toujours être respectée. </li>
<li> J'ai confiance en mon **gouvernement**. </li>
<li> Je n'ai pas à me cacher, je ne fais d'**illégal**.</li>
<li> Je suis prêt à tout pour ma **sécurité**! </li>
<li> Peu importe, tant que ce sont des **métadonnées**.</li>
</ul>
</div>

---

Ai-je des raisons de m'inquiéter ?

*Loading...*

---

Ai-je des raisons de m'inquiéter ?

<font size=40>
*YES, BECAUSE YOU ARE A TERRORIST!*
</font>

![](img/nsa.png)

---

Conclusion
----------

1. On ne détermine pas **soi-même si on est coupable**.
   
2.  Les **règles** peuvent **changer**.
   
3.  Les **lois doivent** pouvoir **ne pas être respectées pour évoluer**.
   
4.  La **vie privée** est un **besoin humain fondamental**.

---

Références
----------

---

-   *[Lettre ouverte à ceux qui n'ont rien à cacher](http://www.internetactu.net/2010/05/21/lettre-ouverte-a-ceux-qui-nont-rien-a-cacher/)*, Jean-Marc Manach
-   *[Big Data : pourquoi nos métadonnées sont-elles plus personnelles  que nos empreintes digitales ?](http://internetactu.blog.lemonde.fr/2013/12/13/big-data-pourquoi-nos-metadonnees-sont-elles-plus-personnelles-que-nos-empreintes-digitales/)*, Hubert Guillaud
-   *["I've Got Nothing to Hide" and Other Misunderstandings of Privacy](http://papers.ssrn.com/sol3/papers.cfm?abstract_id=998565)*, Daniel J. Solove
-   *[La valeur sociale de la vie privée](http://www.internetactu.net/2009/10/21/la-valeur-sociale-de-la-vie-privee/)*,
    Hubert Guillaud
-   *[En 2008, la CNIL a constaté 83% d'erreurs dans les fichiers policiers](http://bugbrother.blog.lemonde.fr/2009/01/21/en-2008-la-cnil-a-constate-83-derreurs-dans-les-fichiers-policiers/)*, Jean-Marc Manach
-   [Émission du 13/01/09 des *Amis d'Orwell*](http://souriez2.lautre.net/mp3/2009/13-01-09.mp3.mp3), Noé Le Blanc
-   *[L'affaire Brandon Mayfield : une surveillance terrifiante](http://www.framablog.org/index.php/post/2014/02/12/le-cas-brandon-mayfield)*, aKa
-   *[Comment la NSA vous surveille (expliqué en patates)](http://www.lemonde.fr/technologies/video/2013/10/21/comment-la-nsa-vous-surveille-explique-en-patates_3499887_651865.html)*, Martin Vidberg et Olivier Clairouin
-   *[Why "I Have Nothing to Hide" Is the Wrong Way to Think About Surveillance](http://www.wired.com/2013/06/why-i-have-nothing-to-hide-is-the-wrong-way-to-think-about-surveillance/)* ([vidéo](https://www.youtube.com/watch?v=eG0KrT6pBPk)), Moxie  Marlinspike
-   Pad Framasoft *[rien-a-cacher](http://lite2.framapad.org/p/rien-a-cacher)*, initié par Luc Didry

</section></section>